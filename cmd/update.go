package main

import (
	"fmt"
	"folder/internal"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func main() {
	err := godotenv.Load("../config/.env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}
	url := os.Getenv("URL")
	spreadsheetId := os.Getenv("SpreadSheetId")

	fmt.Println(url)
	path := "/Users/User/GolandProjects/nft-stars/stars-site"

	internal.UpdateSheet(spreadsheetId, path)
}
