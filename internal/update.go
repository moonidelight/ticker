package internal

import (
	"context"
	"fmt"
	"folder/token"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
	"log"
	"os"
)

func UpdateSheet(spreadsheetId string, path string) {
	ctx := context.Background()

	b, err := os.ReadFile("../config/credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}
	config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
	config.RedirectURL = os.Getenv("Redirect_Uri")
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	c := token.GetClient(config)

	sheetsService, err := sheets.New(c)
	if err != nil {
		log.Fatal(err)
	}

	range2 := "A2:C300"

	valueInputOption := "USER_ENTERED"
	rb := &sheets.ValueRange{
		Values: GetFiles(path),
	}
	fmt.Println("finished")
	resp, err := sheetsService.Spreadsheets.Values.Update(spreadsheetId, range2, rb).ValueInputOption(valueInputOption).Context(ctx).Do()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%#v\n", resp)
}
