package internal

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

func GetJson(path string) []string {
	data, err := os.ReadFile(path)

	if err != nil {
		log.Fatal("Error file:", err)
	}

	var obj map[string]map[string]interface{}

	if err := json.Unmarshal(data, &obj); err != nil {
		log.Fatal("Error json:", err)
	}
	i := 1
	sum := 0
	var res []string
	for k, v := range obj {
		sum += len(v)
		for key := range v {
			fmt.Println(k, key)
			t := k + "." + key
			if key == "mayak" {
				mayak, _ := v[key].(map[string]interface{})
				for m := range mayak {
					res = append(res, t+"."+m)
				}
			} else {
				res = append(res, t)
			}
		}
		i++
	}
	//fmt.Println(sum)
	return res
}
