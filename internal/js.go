package internal

import (
	"io/fs"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

var (
	html   = make(map[string][]string)
	js     = make(map[string][]string)
	prefix = "\\Users\\User\\GolandProjects\\nft-stars\\stars-site\\"
)

func GetFiles(repo_path string) [][]interface{} {

	list := GetJson(repo_path + "/html/locales/ru/translation.json")

	err := filepath.Walk(repo_path, func(path string, f fs.FileInfo, err error) error {
		if !f.IsDir() && filepath.Ext(path) == ".html" {
			Open(path, list, html)
			//htmlFiles = append(htmlFiles, path)
		}
		if !f.IsDir() && filepath.Ext(path) == ".js" {
			Open(path, list, js)
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
	var ans [][]interface{}

	for _, t := range list {
		htmlFile, jsFile := "", ""
		if values, ok := html[t]; ok {
			htmlFile = strings.Join(values, " , ")
		}
		if values, ok := js[t]; ok {
			jsFile = strings.Join(values, " , ")
		}
		ans = append(ans, []interface{}{t, htmlFile, jsFile})
	}
	return ans
}
func Open(path string, list []string, tickers map[string][]string) {
	c, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal("Failed to read file:", err)
	}
	for _, t := range list {
		if strings.Contains(string(c), t) {
			res := strings.TrimPrefix(path, prefix)
			tickers[t] = append(tickers[t], res)
		}
	}
}
